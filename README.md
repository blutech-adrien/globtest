# GlobTest - SOLUTION


## Enoncé

[Echo](https://www.instagram.com/globalisecho/?hl=fr), mascotte de l'équipe de [Globalis](https://www.globalis-ms.com/), a découvert une fonction `foo()` bien mystérieuse. Hélas, il n'a pas accès au code. Curieux et grand amateur de [rétro-ingénierie](https://fr.wikipedia.org/wiki/R%C3%A9tro-ing%C3%A9nierie), Echo s'est amusé à appeler cette fonction, en injectant des données en entrée et en récoltant les sorties. Le comportement de la fonction `foo()` est le suivant :

|  Appel     |  Sortie     |
| ---   |:-:    |
| `foo([[0, 3], [6, 10]])` | `[[0, 3], [6, 10]]` |
| `foo([[0, 5], [3, 10]])` | `[[0, 10]]` |
| `foo([[0, 5], [2, 4]])` | `[[0, 5]]` |
| `foo([[7, 8], [3, 6], [2, 4]])` | `[[2, 6], [7, 8]]` |
| `foo([[3, 6], [3, 4], [15, 20], [16, 17], [1, 4], [6, 10], [3, 6]])` | `[[1, 10], [15, 20]]` |

Le challenge, si vous l'acceptez, serait d'aider Echo à comprendre ce que fait cette fonction et à la recoder. Vous êtes partant ? ;)

### Question 1

Expliquez, en quelques lignes, ce que fait cette fonction.

### Réponse 1

`foo()` est une fontion qui permet de concaténer des tableaux de d'intervalles si une des valeurs d'un intervalle est contenu dans l'intervalle d'un autre. \
Elle créé un nouvel intervalle prenant sur le premier index le plus petit nombre trouvé, et sur le deuxième, le plus grand nombre trouvé.\
Elle prend en paramètre un tableau de tableau de 2 nombres : Array<[number, number]>. \
La fonction retourne un tableau de tous les tbaleaux obtenues trié par ordre croissant.

#### Exemple

On a le tableau suivant : `[[1, 6], [2, 8], [9, 11], [2, 6]]`. \
Pour le premier tableau, on vérifie si 1 ou 6 peut être contenue dans l'intervalle d'un autre tableau. \
Ici, ces 2 nombres sont contenue dans l'intervale des tableaux `[2, 8]` et `[2, 6]`. \
Ce qui nous donne le tableau `[1, 8]`. \
On fait la même chose pour tous les tableaux restant, ce qui nous donne : `[[1, 8], [9, 11]]`.

### Question 2

Codez cette fonction.
Merci de fournir un fichier contenant :

- la fonction, 
- l'appel de la fonction, avec un jeu de test en entrée,
- l'affichage du résultat en sortie.

### Réponse 2

Executer le script `foo.js` en ligne de commande :
```bash
  node foo.js '<param>'
```
Avec `'<param>'` un tableau d'intervales.

#### Exemple

```bash
  node foo.js '[[1, 6], [2, 8], [9, 11], [2, 6]]'
```

### Question 3

Précisez en combien de temps vous avez implémenté cette fonction.

### Réponse 3

Compréhension et rétro-ingénierie : 20 minutes. \
Implémentation : 30 minutes.

## Merci

Par avance, un grand merci pour le temps que vous consacrerez à ce challenge, en espérant vous voir rejoindre très prochainement [nos équipes](https://www.globalis-ms.com/jobs/) ;) 