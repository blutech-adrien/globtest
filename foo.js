const args = process.argv;

/**
 * Given an array of intervals, merge all overlapping intervals
 * @param array - an array of intervals.
 * @returns The new array of intervals.
 */
function foo(array) {
    const newArray = [array[0]];
    let index = 0;
    while (index < array.length) {
        const element = array[index];
        let find = false;
        newArray.forEach(na => {
            if (na[0] <= element[0] && na[1] >= element[0] || na[0] <= element[1] && na[1] >= element[1]) {
                na[0] = na[0] > element[0] ? element[0] : na[0];
                na[1] = na[1] < element[1] ? element[1] : na[1];
                array.splice(index, 1);
                find = true;
            }
        });
        if (!find) { newArray.push(element); index += 1; }
    }
    return newArray;
}

/**
 * Given an array, check if the third argument if an array of intervals and call foo() if true.
 * If false, print usage.
 * @param args - The arguments passed to the script.
 */
function main(args) {
    if (args.length === 3) {
        const array = JSON.parse(args[2]);
        if (array.length > 0) console.log(foo(array))
        else console.log([])
    } else {
        console.error(`
            Usage :
            node foo.js '<param>'
            With <param> as array of numbers array : '[[0, 5], [2, 9]]'
        `);
    }
}

main(args);